package com.askellio.testtemp;

import android.Manifest;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.DragAndDropPermissions;
import android.view.DragEvent;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("zzz", "before");
        Log.i("zzz", "fine "+String.valueOf(ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PERMISSION_GRANTED));
        Log.i("zzz", "coarse"+String.valueOf(ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PERMISSION_GRANTED));

        Log.i("zzz", "checker fine");
        Log.i("zzz", "granted "+String.valueOf(PermissionChecker.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PermissionChecker.PERMISSION_GRANTED));
        Log.i("zzz", "denied "+String.valueOf(PermissionChecker.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PermissionChecker.PERMISSION_DENIED));
        Log.i("zzz", "denied_app_op "+String.valueOf(PermissionChecker.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PermissionChecker.PERMISSION_DENIED_APP_OP));

        Log.i("zzz", "checker coarse");
        Log.i("zzz", "granted "+String.valueOf(PermissionChecker.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PermissionChecker.PERMISSION_GRANTED));
        Log.i("zzz", "denied "+String.valueOf(PermissionChecker.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PermissionChecker.PERMISSION_DENIED));
        Log.i("zzz", "denied_app_op "+String.valueOf(PermissionChecker.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PermissionChecker.PERMISSION_DENIED_APP_OP));

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);


        Log.i("zzz", String.valueOf(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)));



            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.i("zzz", "location");
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                    Log.i("zzz", "status");
                }

                @Override
                public void onProviderEnabled(String provider) {
                    Log.i("zzz", "provider enabled");
                }

                @Override
                public void onProviderDisabled(String provider) {
                    Log.i("zzz", "provider disabled");
                }
            });
        Log.i("zzz", "after");
        Log.i("zzz", "fine "+String.valueOf(ContextCompat.checkSelfPermission(
        this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PERMISSION_GRANTED));
        Log.i("zzz", "coarse "+String.valueOf(ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PERMISSION_GRANTED));

        Log.i("zzz", "checker fine");
        Log.i("zzz", "granted "+String.valueOf(PermissionChecker.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PermissionChecker.PERMISSION_GRANTED));
        Log.i("zzz", "denied "+String.valueOf(PermissionChecker.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PermissionChecker.PERMISSION_DENIED));
        Log.i("zzz", "denied_app_op "+String.valueOf(PermissionChecker.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PermissionChecker.PERMISSION_DENIED_APP_OP));

        Log.i("zzz", "checker coarse");
        Log.i("zzz", "granted "+String.valueOf(PermissionChecker.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PermissionChecker.PERMISSION_GRANTED));
        Log.i("zzz", "denied "+String.valueOf(PermissionChecker.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PermissionChecker.PERMISSION_DENIED));
        Log.i("zzz", "denied_app_op "+String.valueOf(PermissionChecker.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PermissionChecker.PERMISSION_DENIED_APP_OP));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i("zzz", "permission result");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i("zzz", "permission result");
    }

    @Override
    public DragAndDropPermissions requestDragAndDropPermissions(DragEvent event) {
        Log.i("zzz", "drag and drop");
        return super.requestDragAndDropPermissions(event);
    }


}
